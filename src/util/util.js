import createHistory from 'history/createHashHistory'

export function redirect(link){
  let x = createHistory()
  x.push(link)
}

export function NumericEntry(event){
      const keyCode = event.keyCode || event.which;
      const keyValue = String.fromCharCode(keyCode);
      if(isNaN(keyValue)){
        event.preventDefault()
      }
}
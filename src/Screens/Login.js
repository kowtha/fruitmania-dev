import React from 'react';
import './Login.scss';
import Grid from '@material-ui/core/Grid';
import drag4 from '../images/dragon-04.jpg'
import Input from '../Components/Input'
import logo from '../images/welcome_logo.png'
import divider from '../images/img-2.png'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import custardApple from '../images/custard-apple.jpg'
import { withStyles } from '@material-ui/core/styles'
import Slide from '@material-ui/core/Slide'
const showToast = require('../Components/Toast').showToast
window.x = showToast
const styles = theme => ({
      indicator: {
        backgroundColor: "rgb(123,50,0)",
     },
  
      '&:hover': {
            color: 'rgb(123,50,0)',
            opacity: 1,
          },
          '&$selected': {
            color: 'rgb(123,50,0)',
            fontWeight: theme.typography.fontWeightMedium,
          },
          '&:focus': {
            color: 'rgb(123,50,0)',
          }
     
})
const Login = (props) =>{
      const LoginForm = () => {

            return <Slide direction="right" timeout={200} in={checked} mountOnEnter unmountOnExit><div>
                        <div>
                              <Input className="row" placeholder="Mobile Number"/>
                        </div>
                        <div>
                              <Input className="row" placeholder="Password" type="password"/>
                        </div>
                        <Button className="SubmitButton row" onClick={() => {props.history.push('/Portal/Home')}} variant="contained">Login</Button>    
                  </div>
                  </Slide>
      }
            
      const OTP = () => {
            let [timer, setTimer] = React.useState(180)
            setTimeout(() => {
                  setTimer(timer-1)
            },1000)
             
            return <Slide direction="" timeout={200} in={checked} mountOnEnter unmountOnExit>
                        <div>
                              <div>
                                    <Input type="otp" length={5}/>
                              </div>
                              <div>
                                    <label>You will receive an OTP to your mobile number<br></br>+91xxxxxxx90 <b>0{Math.floor(timer / 60)} : {timer - Math.floor(timer / 60) * 60 }</b></label>
                              </div>
                              <Button className="SubmitButton row" onClick={() => setValue(2)} variant="contained" >Register</Button>    
                        </div>
                  </Slide>
      }
      const Register = () => {
            if(value === 2){
                  return <OTP />
            }
            return <Slide direction="right" timeout={100} in={checked} mountOnEnter unmountOnExit>
                        <div>
                              <div>
                                    <Input className="row" placeholder="Mobile Number"/>
                              </div>
                              <Button className="SubmitButton row" onClick={() => setValue(2)} variant="contained" >Register</Button>    
                        </div>
                  </Slide>
      }

      
      const [value, setValue] = React.useState(0);

      function handleChange(event, newValue) {
            setValue(newValue);
      }
      const {classes} = props
      const checked = true
      const enableToast = {type: "error", message: "Invalid Username or Password", status: true}
      return (

            <Grid container>
                  <Grid  className="LoginForm" item md={6} sm={12}>
                        <img alt=""  src={logo} width="350" height="250"/>
                        <img alt="" className="divider" src={divider} />
                        {(value < 2) ? <Tabs
                              value={value}
                              classes={classes}
                              onChange={handleChange}
                              indicatorColor={"primary"}
                              textColor="primary"
                              centered>
                              <Tab label="Login" className="subTabs" />
                              <Tab label="Register" />
                        </Tabs> : ""}
                        {(enableToast && enableToast.status) ? 
                              showToast(enableToast.type, enableToast.message)
                         : ""}

                              {value === 0 ? <LoginForm />: <Register />}
                  
                  </Grid>
                  <Grid className="ImageContainer" item md={6} sm={12} >
                        <img alt="" src={value === 0 ? drag4 : custardApple} width="100%" height="100%"/>
                  </Grid>
            </Grid>
      );
}

export default withStyles( styles )(Login);

import React from 'react'
import Header from '../Components/Header'
import Dashboard from './Dashboard'
import {Home} from './Home'
import {Packages} from './Packages'
import {AboutUs} from './AboutUs'
import './Login.scss'
import { faHistory,faAppleAlt, faGift, faInfoCircle, faChartLine, faPowerOff, faPhoneSquare } from '@fortawesome/free-solid-svg-icons';

import {Switch,  HashRouter , Route, withRouter} from 'react-router-dom'

const redirect = require("../util/util").redirect

export default class Welcome extends React.Component{
      render(){
            const dropDownData = [
                  {label: "My Dashboard", icon: faChartLine, action: () => redirect("/Portal/Dashboard")}, 
                  {label: "Orders", icon: faHistory, action: () => redirect("/Portal/Orders")}, 
                  {label: "Fruits", icon: faAppleAlt, action: () => redirect("/Portal/Fruits")}, 
                  {label:"Special Packages", icon: faGift,action: () => redirect("/Portal/SpecialPackages")},  
                  {label:"About Us", icon: faInfoCircle}, 
                  {label: "Contact Us", icon: faPhoneSquare},
                  {label: "Log Out", icon: faPowerOff, action: () => redirect("/")}
            ]
            return <div>
                  <HashRouter basename="/Portal" >
                        <div className="NavBarSection">
                              <Header dropDownItems={dropDownData}/>
                        </div>
                        <Switch>
                              <Route path="/Home" exact component={withRouter(Home)}/>
                              <Route path="/Packages" exact component={withRouter(Packages)}/>
                              <Route path="/Aboutus" exact  component={withRouter(AboutUs)}/>
                              <Route path="/Dashboard" exact component={withRouter(()=><Dashboard Navitems={dropDownData} tab={'My Dashboard'}/>)}/>
                              <Route path="/Fruits" exact component={withRouter(()=><Dashboard Navitems={dropDownData} tab={'Fruits'}/>)}/>
                              <Route path="/Orders" exact component={withRouter(()=><Dashboard Navitems={dropDownData} tab={'Orders'}/>)}/>
                              <Route path="/SpecialPackages" exact component={withRouter(()=><Dashboard Navitems={dropDownData} tab={'Special Packages'}/>)}/>
                              <Route path="/Feedback" exact component={withRouter(()=><Dashboard Navitems={dropDownData} tab={'Feedback'}/>)}/>
                        </Switch>
                  </HashRouter>
 
            </div>
      }
}
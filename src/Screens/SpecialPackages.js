import React from 'react'
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


export default class SpecialPackages extends React.Component{
      render(){
            const card = (content) =>{
                  return <Paper style={{width: "300px", padding: '30px', borderRadius: '10px', float: 'left', marginRight: '10px'}}>
                        <h2 style={{color: 'rgb(123,50,0)'}}>Rs. {content.price}</h2>
                        <Divider variant="middle" style={{ backgroundColor: 'rgb(123,50,0)',borderRadius: '90px',height: '2px'}}/>
                        <Typography paragraph style={{fontWeight: 'bold', margin: '20px'}}>
                              Dragon Fruit(RED)  <br />
                              1 KG
                        </Typography>
                        <Button className="OrderButton" >Order</Button>
                 </Paper>

            }
            return <div style={{position: "relative",
                  float: "left",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)" }}>
                  {card({price: '500'})}
                  {card({price: '500'})}
            </div>
      }
}
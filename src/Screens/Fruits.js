import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faTemperatureHigh, faUmbrella, faLeaf, faSnowflake} from '@fortawesome/free-solid-svg-icons';


export default class Fruits extends React.Component{
      render(){
            return <div>
                  <AppBar position="static" style={{ backgroundColor: "white"}}>
            <Tabs
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth">
              <Tab label="Summer Fruits"  style={{color: "#FF4500", fontWeight: 'bold'}} icon={<FontAwesomeIcon icon={faTemperatureHigh} size={'2x'} />}/>
              <Tab label="Monsoon Fruits" style={{color: "#00bfff", fontWeight: 'bold'}} icon={<FontAwesomeIcon icon={faUmbrella} size={'2x'} />} />
              <Tab label="Autumn Fruits" style={{color: "green", fontWeight: 'bold'}} icon={<FontAwesomeIcon icon={faLeaf} size={'2x'} />}/>
              <Tab label="Winter Fruits" style={{color: "royalblue", fontWeight: 'bold'}} icon={<FontAwesomeIcon icon={faSnowflake} size={'2x'} />}/>
            </Tabs>
          </AppBar></div>
      }
}
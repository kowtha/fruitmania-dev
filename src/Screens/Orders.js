import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

export default class Orders extends React.Component{
      render(){
            return <div>
                  <Paper style={{width: "75%", marginLeft: '12.5%',borderRadius: '20px', marginTop: "20px"}}>
                        <Table>
                              <TableHead>
                                    <TableRow >
                                          <TableCell align={'center'} style={{fontSize: "14px", fontWeight: 'bold'}}>
                                                Order Date            
                                          </TableCell>
                                          <TableCell align={'center'} style={{fontSize: "14px", fontWeight: 'bold'}}>
                                                Payment            
                                          </TableCell>
                                          <TableCell align={'center'} style={{fontSize: "14px", fontWeight: 'bold'}}>
                                                Order Id    
                                          </TableCell>
                                          <TableCell align={'center'} style={{fontSize: "14px", fontWeight: 'bold'}}>
                                                Address            
                                          </TableCell>
                                    </TableRow>
                              </TableHead>
                              <TableBody>
                                    <TableRow >
                                          <TableCell align={'center'} style={{fontSize: "14px"}}>
                                                <b>RS. 150<br/>
                                                Price Package<br/>
                                                Dragon Fruit (RED) 1 KG<br/>
                                                Dragon Fruit (RED) 2 KG
                                                </b>
                                          </TableCell>
                                          <TableCell align={'center'} style={{fontSize: "14px"}}>
                                                <b>Pay on delivery 580/- </b>       
                                          </TableCell>
                                          <TableCell colSpan={2} align={'center'} style={{fontSize: "14px"}}>
                                                Not Yet Delivered   
                                          </TableCell>
                                        
                                    </TableRow>
                              </TableBody>
                        </Table>
                  </Paper>
            </div>
      }
}
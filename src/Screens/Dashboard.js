import React from 'react'
import Grid from '@material-ui/core/Grid';
import './Dashboard.scss'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Fruits from './Fruits'
import Orders from './Orders'
import SpecialPackages from './SpecialPackages'


export default class Dashboard extends React.Component{
      constructor(props){
            super(props)
            this.state={
                  "Fruits" : <Fruits />,
                  "Orders" : <Orders />,
                  "Special Packages": <SpecialPackages />
            }
      }

      
      render(){
            return <div>
                        <Grid container >
                              <Grid item md={3} className="LeftNav">
                                    <List component="nav" >
                                          {
                                                this.props.Navitems.map((ele) =>{
                                                      return <ListItem button key={ele.label} onClick={ele.action} style={{color: this.props.tab === ele.label ? '#3A5F0B' : ''}}>
                                                                  <ListItemIcon className="listIcon" style={{color: this.props.tab === ele.label ? '#3A5F0B' : ''}}>
                                                                        <FontAwesomeIcon icon={ele.icon} />
                                                                  </ListItemIcon>
                                                                  <ListItemText primary={<label style={{fontWeight: 'bold', marginLeft: '-15px', cursor: 'pointer'}}>{ele.label}</label>} />
                                                            </ListItem>
                                                })
                                          }
                                    </List>
                              </Grid>
                              <Grid item md={9} className="dashBoardBody"  justify = "center">
                                    {this.state[this.props.tab]}
                              </Grid>
                        </Grid>
                  </div>
      }
}


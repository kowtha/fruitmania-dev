import React from 'react';
import './App.css';
import {HashRouter as Router, Route, withRouter} from 'react-router-dom'
import Login from './Screens/Login'
import Welcome from './Screens/Welcome'

function App() {
  return (
    <Router>
      <Route path="/" exact component={withRouter(Login)} />
      <Route path="/Portal/*" exact component={withRouter(Welcome)} />
    </Router>
  );
}
export default App;

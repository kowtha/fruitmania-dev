import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Dropdown(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }
  return (
    <div className = {(props.className) ? props.className : ""}>
      <div onClick={handleClick}>
            {props.title}
      </div>
      <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {props.data.map((ele) => <MenuItem onClick={ele.action} ><FontAwesomeIcon icon={ele.icon}/> <span className="menuItem">{ele.label}</span></MenuItem> )}
      </Menu>
    </div>
  );
}
export default Dropdown;

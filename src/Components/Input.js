import React from 'react'
import './Components.scss'
const util = require('../util/util')

const Input = (props) => {
    let className = "input-box "
    if(props.className){
        className += props.className || ""
    }

    function handleClick(event){
        event.preventDefault()
    }
    window.x = util
    if(props.type=== "otp"){
        let opt = <div>
        {Array.from(Array(props.length)).map(
            (ele, index) =>{return <input type="text" onChange={handleClick} pattern="[\d]{9}" className="input-box otpInput" key={index} maxLength={1}/>}
        )}
        
    </div>
        return opt
    }
    return <input  {...props}  className={className} />
}

export default Input
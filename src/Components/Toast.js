import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faExclamationTriangle, faCheckCircle, faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

class Toast extends React.Component{
      constructor(props){
            super(props)
            this.state = {
                  display: true,
                  error: {color: "#d32f2f", icon: faExclamationTriangle},
                  warning: {color: "#ffa000", icon: faTimesCircle},
                  success: {color: "#43a047", icon: faCheckCircle}
            }
      }

      showToast = (type, message) =>{
            this.setState({display: true})
            return <Toast type={type}  message={message} />
      }

      hideToast = () =>{
            this.setState({display: false})
      }

      render(){
            setTimeout(this.hideToast, 4000)
            return <div className="Toast" style={{display: this.state.display ? "block" : "none", backgroundColor: this.state[this.props.type].color}} >
                  <FontAwesomeIcon icon={this.state[this.props.type].icon}/>  <label style={{marginLeft: "5px"}}>{this.props.message}</label> <label style={{marginLeft: "20px", cursor: 'pointer'}}><FontAwesomeIcon onClick={this.hideToast} icon={faTimes} /></label> 
            </div>
      }
}


export const showToast = (type, message)=>{
      return <Toast type={type} message={message} />
}



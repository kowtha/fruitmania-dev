import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import logo from '../images/logo.svg'
import {Link} from 'react-router-dom'
import Dropdown from './Dropdown'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faUserCircle,faHandHoldingHeart } from '@fortawesome/free-solid-svg-icons';
import Tooltip from '@material-ui/core/Tooltip'
import Button from '@material-ui/core/Button'
import './Components.scss'
class Header extends React.Component {
      constructor(props){
            super(props)
            this.state = {
                  disableNavSticky : false
            }
            window.addEventListener("scroll", () =>{
                  if(window.pageYOffset > 0){
                        this.setState({disableNavSticky: true})

                  }
                  else{
                        this.setState({disableNavSticky: false})          
                }
            })
      }
      render(){

            return (
              <div>
                <AppBar className={(this.state.disableNavSticky) ? "navBar" : "navBar disableNavSticky"}  position="static">
                  <Toolbar>
                      <img alt="" className="navLogo" src={logo} />
                      <div className="navLink">
                              <Button className="SubmitButton PrimaryButton" ><Link to="/Home"  color="inherit">Home</Link></Button>
                              <Button className="SubmitButton PrimaryButton" ><Link to="/Packages" color="inherit">Packages</Link></Button>
                              <Button className="SubmitButton PrimaryButton" ><Link to="/Aboutus" color="inherit">About Us</Link></Button>
                      </div>
                        <Tooltip title={"Gratitude"}>
                              <div>
                                    <FontAwesomeIcon className="navRight" style={{cursor: "pointer"}} icon={faHandHoldingHeart} size={'lg'}/>
                              </div>
                        </Tooltip>      
                        <Dropdown className="navRight" title={<FontAwesomeIcon style={{cursor: "pointer", margin: "0px 35px 0px 25px"}}  icon={ faUserCircle } size={'2x'} />} data={this.props.dropDownItems} />
                  </Toolbar>
                </AppBar>
            </div>
            );
      }
  
}

export default (Header);

